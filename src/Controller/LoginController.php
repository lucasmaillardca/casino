<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserFormType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Console\EntityManagerProvider;
use http\Env\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;


class LoginController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */

    public function show(Environment $twig, Request $request, EntityManagerInterface $entityManager): Response
    {
        $user = new User();

        $form= $this->createForm(UserFormType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $entityManager->persist();
            $entityManager->flush();

            return new Response('Bonjour ' . $user->getFirstName() . ', votre compte a bien été créé.');
        }

        return $this->render('login/index.html.twig',[
            'userForm' => $form->createView()
        ]);
    }
}
